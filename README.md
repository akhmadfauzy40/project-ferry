## Tutor Instalasi Program

- Buka direktori tempat anda ingin menyimpan project, lalu buka terminal di direktori tersebut
- ketik perintah di bawah

```bash
git clone https://gitlab.com/akhmadfauzy40/project-ferry.git
```

- masuk ke folder project dengan mengetik perintah di bawah ini pada terminal

```bash
cd project-ferry
```

- ketik perintah di bawah ini untuk membuka codingan

```bash
start Program.cs
```

- lalu jalankan program dengan mengetik perintah di bawah ini

```bash
dotnet run --project .\Project-Ferry.csproj
```

