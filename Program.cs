﻿// See https://aka.ms/new-console-template for more information
// Console.WriteLine("Hello, World!");

using System;

class Ukuran
{
    private double panjang; // Satuan panjang dalam mm

    public void setUkuran(double nilaiPanjang)
    {
        panjang = nilaiPanjang;
    }

    public double getUkuranMM()
    {
        return panjang;
    }

    public double getUkuranCM()
    {
        return panjang / 10; // 1 cm = 10 mm
    }

    public double getUkuranM()
    {
        return panjang / 1000; // 1 m = 1000 mm
    }

    public void tambahkan()
    {
        panjang += 30; // Menambah panjang sebanyak 3 cm (1 cm = 10 mm)
    }

    public void kurangi()
    {
        panjang -= 30; // Mengurangi panjang sebanyak 3 cm (1 cm = 10 mm)
    }
}

class Program
{
    static void Main()
    {
        Ukuran penggaris = new Ukuran();
        Ukuran meteran = new Ukuran();

        // Set ukuran awal
        penggaris.setUkuran(150); // 150 mm (UBAH SAJA JIKA PERLU UNTUK SETTING UKURAN PENGGARIS)
        meteran.setUkuran(3000); // 3000 mm (UBAH SAJA JIKA PERLU UNTUK SETTING UKURAN METERAN)

        // Tampilkan ukuran awal
        Console.WriteLine("Ukuran Penggaris:");
        TampilkanUkuran(penggaris);

        Console.WriteLine("\nUkuran Meteran:");
        TampilkanUkuran(meteran);

        // Lakukan operasi penambahan dan pengurangan beberapa kali (salin saja line 60-69 lalu paste setelah operasi kedua dan lakukan perubahan)
        Console.WriteLine($"\n=============================");
        Console.WriteLine("Operasi ke-1:");
        // Tambahkan 3 cm
        penggaris.tambahkan();
        Console.WriteLine("Tambah 3 cm pada Penggaris:");
        TampilkanUkuran(penggaris);
        // Kurangi 3 cm
        meteran.kurangi();
        Console.WriteLine("Kurangi 3 cm pada Meteran:");
        TampilkanUkuran(meteran);

        Console.WriteLine($"\n=============================");
        Console.WriteLine("Operasi ke-2:");
        // Kurangi 3 cm
        penggaris.kurangi();
        Console.WriteLine("Kurangi 3 cm pada Penggaris:");
        TampilkanUkuran(penggaris);
        // Tambahkan 3 cm
        meteran.tambahkan();
        Console.WriteLine("Tambahkan 3 cm pada Meteran:");
        TampilkanUkuran(meteran);

    }

    static void TampilkanUkuran(Ukuran objekUkuran)
    {
        Console.WriteLine($"Panjang (mm): {objekUkuran.getUkuranMM()} mm");
        Console.WriteLine($"Panjang (cm): {objekUkuran.getUkuranCM()} cm");
        Console.WriteLine($"Panjang (m): {objekUkuran.getUkuranM()} m");
    }
}
